# Visualizing transformer image understanding

- The goal of this quick project is to display the Attention map from a finetuned (27k images of benchmark dataset EuroSAT RGB from Sentinel-2) ViT architecture.
- This map shows the more or less important parts in the satellite images.
- This information naturally reflects the features which are useful to determine the image nature, or class


- Enabling an explainable AI decision can create trust even in non-critical applications so I wanted to try out a kind of local interpretability with a modern architecture that I love (i.e transformers)

![Original image](data/river.png) ![Attention map](data/river_attention_map.png)



