import torch
import torch.nn as nn

class PatchEmbed(nn.Module):
        """Split images into patches et les intègrent avec leur position et projections linéaires
        
        Arguments:
        img size, patch size, nombre de channels (bandes) d'entrée, dimensions de projection
        64, 8, 3, 512
        
        pour patch taille 8 comparer perf avec poids timm 
        https://github.com/rwightman/pytorch-image-models/blob/master/timm/models/vision_transformer.py
        
        Attributs:
        n_patches
        proj : nn.Conv2D couche de convolution qui fait le découpage, et la projection linéaire
        """
        def __init__(self, img_size, patch_size, in_chans=3, embed_dim=512):
            super().__init__()
            self.img_size = img_size
            self.patch_size = patch_size
            self.n_patches = (img_size // patch_size)**2
            
            
            
            
            self.proj = nn.Conv2d(
                in_chans, 
                embed_dim, 
                kernel_size = patch_size,
                stride = patch_size
            )
        def forward_pass(self, x):
            """
            parametres (n images): 
            x : torch.Tensor
                Shape '(n_samples, in_chans, img_size, img_size)'
                
            Retourne n sequences de patches:
            x : torch.Tensor 
                Shape '(n_samples, n_patches, embed_dim)'
            
            """
            x = self.proj(x)
            #n_samples, embed_dim, patch_size**0.5, patch_size**0.5
            x = x.Flatten(2) #n_samples,  embed_dim, n_patches soit n images en 64 lignes de 64 patches
            
            x = x.transpose(1, 2) #n_samples, n_patches, embed_dim soit n images en 64 lignes de patches
            
            return x



#l'attention prend les embeddings de dimension 512 (projection avec pos des tokens de patch 8x8)
#les multiplie par les poids pr obtenir q k v de dimensions 64 
#Et ce 8 fois car 8 tetes
#puis les 8 Z qui sont concat: les tokens sont considérés comme encodés

#donc entrée: matrice embeddings des tokens,
#matrices poids q k v 
#sortie: matrice où chaque ligne est les scores d'attention de "chaque patch embedding", donc mat de shape [tokens, embeddings pondérés]
#https://arxiv.org/pdf/2010.11929.pdf

class Attention(nn.Module):
    """Mécanisme d'Attention.
    
    Parametres
    
    dim : int
        dimensions entree et sortie des features d'un token (element de la seq)
        512 ici les embeddings ?
    
    n_heads: int
        Nombre de tetes d'attention
    
    qkv_bias: bool
        Si on rajoute un biais aux projections des query keys et values
    
    attn_p: float
        probabilité passée au Dropout (dropout pdt training et pas test)
        (Retire moitié du tenseur et chaque scalaire du tenseur*1/1-p)
    
    Attributs:
    
    scale : float
        Constante de normalisation du produit scalaire (racine de dk)
    qkv: nn.Linear
        Projection (application linéaire) pour les query key et value
    proj: nn.Linear
        Mapping linéaire qui prend le résultat concaténé de toutes les tetes d'attention
        et qui les map sur un nouvel espace
    
    attn_drop, proj_drop : nn.Dropout
        Couches de dropout
    
    """
    
    def __init__(self, dim=512, n_heads = 8, qkv_bias = True, attn_p = 0., proj_p=0.):
        super().__init__()
        self.n_heads = n_heads #8 tetes
        self.dim = dim #embeddings 512?
        self.head_dim = dim // n_heads #512/8 = 64 ??
        #on definit comme ça la dimensionalité pour chacune des tetes 
        #car une fois qu'on concatène toutes les tetes d'attentions,
        #on obtient un nouveau tenseur qui a les mêmes dim que l'entrée cad 512
        self.scale = self.head_dim**-0.5 #1/sqrt(dk) pour pas avoir de grands valeurs ds softmax, pr pas faire disparaitre gradient
        
        #embeddings *W. En input 2D de Linear il y a le batch cad n_samples, et les colonnes features cad dim
        self.qkv = nn.Linear(dim, dim*3, bias=qkv_bias)
        
        self.attn_drop = nn.Dropout(attn_p)
        self.proj = nn.Linear(dim, dim) # matmul avec WO qui donne tout les z concaténés
        self.proj_drop = nn.Dropout(proj_p)
        
    
    def forward(self, x):
        """Passage à travers l'architecture
        
        Parametre
        
        x : torch.Tensor
            Shape '(n_samples, n_patches+1, dim)'
            27k images, 1 class token+8 tokens de patches de 512 colonnes
            
        Retourne
        
        torch.Tensor
            Shape '(n_samples, n_patches+1, dim)'
        """
        n_samples, n_tokens, dim = x.shape
        
        if dim!= self.dim:
            print("La dimension n'est pas la meme que ds le constructeur")
            raise ValueError
        
        qkv = self.qkv(x) # (n_samples, n_patches+1, dim*3) on mappe
        qkv = qkv.reshape(
                n_samples, n_tokens, 3, self.n_heads, self.head_dim
        ) # on crée une dimension supplémentaire pour le nombre de tetes, et 3 autres pour les q k et v
        # (n_samples, n_patches+1, 3, n_heads, head_dim)
        qkv = qkv.permute(
            2, 0, 3, 1, 4
        ) #(3, n_samples, n_heads, n_patches+1, head_dim)
        
        q, k, v = qkv[0], qkv[1], qkv[2]
        
        k_t = k.transpose(-2, -1) # swap l'avant derniere dimension avec la derniere
        # exemple: avec un matrice l'avant derniere est 0 et la derniere 1
        # (n_samples, n_heads, head_dim, n_patches+1)
        
        dp = (
            q @ k_t
        ) * self.scale # (n_samples, n_heads, n_patches + 1, n_patches + 1)
        
        attn = dp.softmax(dim=-1) # (n_samples, n_heads, n_patches+1, n_patches+1)
        #attention score = softmax(dot product), on obtient la distrib des pondérations
        attn = self.attn_drop(attn)
        
        weighted_avg = attn @ v # (n_samples, n_heads, n_patches+1, head_dim) pondérations x values
        
        
        
        weighted_avg = weighted_avg.transpose(1, 2) # (n_samples, n_patches+1, n_heads, head_dim)
        
        weighted_avg = weighted_avg.flatten(2) # (n_samples, n_patches+1, dim)
        
        x = self.proj(weighted_avg) # (n_samples, n_patches +1, dim) "Zfinal" = transfo linéaire qui concat les Z, matmul par WO
        x = self.proj_drop(x) # (n_samples, n_patches +1, dim)
        
        return x
    
    
    



class MLP(nn.Module):
    """Perceptron multi couche
    
    Parametres
    ----------
    in_features : int
        Nombre d'input features.
    hidden_features : int
        Number of nodes in the hidden layer.
    out_features : int
        Number of output features.
    p : float
        Proba pour dropout 
    Attributes
    ----------
    fc : nn.Linear
        The First linear layer.
    act : nn.GELU
        GELU activation function.
    fc2 : nn.Linear
        The second linear layer.
    drop : nn.Dropout
        Dropout layer.
    """
    def __init__(self, in_features, hidden_features, out_features, p=0.):
        super().__init__()
        self.fc1 = nn.Linear(in_features, hidden_features)
        self.act = nn.GELU() #Gaussian Error Linear Unit
        self.fc2 = nn.Linear(hidden_features, out_features)
        self.drop = nn.Dropout(p)

    def forward(self, x):
        """Run forward pass.
        Parameters
        ----------
        x : torch.Tensor
            Shape `(n_samples, n_patches + 1, in_features)`.
        Returns
        -------
        torch.Tensor
            Shape `(n_samples, n_patches +1, out_features)`
        """
        x = self.fc1(
                x
        ) # (n_samples, n_patches + 1, hidden_features)
        x = self.act(x)  # (n_samples, n_patches + 1, hidden_features)
        x = self.drop(x)  # (n_samples, n_patches + 1, hidden_features)
        x = self.fc2(x)  # (n_samples, n_patches + 1, out_features)
        x = self.drop(x)  # (n_samples, n_patches + 1, out_features)

        return x


class Block(nn.Module):
    """Transformer block.
    Parameters
    ----------
    dim : int
        Embedding dimension.
    n_heads : int
        Number of attention heads.
    mlp_ratio : float
        Determines the hidden dimension size of the `MLP` module with respect
        to `dim`.
    qkv_bias : bool
        If True then we include bias to the query, key and value projections.
    p, attn_p : float
        Dropout probability.
    Attributes
    ----------
    norm1, norm2 : LayerNorm
        Layer normalization.
    attn : Attention
        Attention module.
    mlp : MLP
        MLP module.
    """
    def __init__(self, dim, n_heads, mlp_ratio=4.0, qkv_bias=True, p=0., attn_p=0.):
        super().__init__()
        self.norm1 = nn.LayerNorm(dim, eps=1e-6)
        self.attn = Attention(
                dim,
                n_heads=n_heads,
                qkv_bias=qkv_bias,
                attn_p=attn_p,
                proj_p=p
        )
        self.norm2 = nn.LayerNorm(dim, eps=1e-6)
        hidden_features = int(dim * mlp_ratio)
        self.mlp = MLP(
                in_features=dim,
                hidden_features=hidden_features,
                out_features=dim,
        )

    def forward(self, x):
        """Run forward pass.
        Parameters
        ----------
        x : torch.Tensor
            Shape `(n_samples, n_patches + 1, dim)`.
        Returns
        -------
        torch.Tensor
            Shape `(n_samples, n_patches + 1, dim)`.
        """
        x = x + self.attn(self.norm1(x))
        x = x + self.mlp(self.norm2(x))

        return x


class VisionTransformer(nn.Module):
    """Simplified implementation of the Vision transformer.
    Parameters
    ----------
    img_size : int
        Both height and the width of the image (it is a square).
    patch_size : int
        Both height and the width of the patch (it is a square).
    in_chans : int
        Number of input channels.
    n_classes : int
        Number of classes.
    embed_dim : int
        Dimensionality of the token/patch embeddings.
    depth : int
        Number of blocks.
    n_heads : int
        Number of attention heads.
    mlp_ratio : float
        Determines the hidden dimension of the `MLP` module.
    qkv_bias : bool
        If True then we include bias to the query, key and value projections.
    p, attn_p : float
        Dropout probability.
    Attributes
    ----------
    patch_embed : PatchEmbed
        Instance of `PatchEmbed` layer.
    cls_token : nn.Parameter
        Learnable parameter that will represent the first token in the sequence.
        It has `embed_dim` elements.
    pos_emb : nn.Parameter
        Positional embedding of the cls token + all the patches.
        It has `(n_patches + 1) * embed_dim` elements.
    pos_drop : nn.Dropout
        Dropout layer.
    blocks : nn.ModuleList
        List of `Block` modules.
    norm : nn.LayerNorm
        Layer normalization.
    """
    def __init__(
            self,
            img_size=64,
            patch_size=8,
            in_chans=3,
            n_classes=10,
            embed_dim=512,
            depth=2,
            n_heads=8,
            mlp_ratio=4.,
            qkv_bias=True,
            p=0.,
            attn_p=0.,
    ):
        super().__init__()

        self.patch_embed = PatchEmbed(
                img_size=img_size,
                patch_size=patch_size,
                in_chans=in_chans,
                embed_dim=embed_dim,
        )
        self.cls_token = nn.Parameter(torch.zeros(1, 1, embed_dim))
        self.pos_embed = nn.Parameter(
                torch.zeros(1, 1 + self.patch_embed.n_patches, embed_dim)
        )
        self.pos_drop = nn.Dropout(p=p)

        self.blocks = nn.ModuleList(
            [
                Block(
                    dim=embed_dim,
                    n_heads=n_heads,
                    mlp_ratio=mlp_ratio,
                    qkv_bias=qkv_bias,
                    p=p,
                    attn_p=attn_p,
                )
                for _ in range(depth)
            ]
        )

        self.norm = nn.LayerNorm(embed_dim, eps=1e-6)
        self.head = nn.Linear(embed_dim, n_classes)


    def forward(self, x):
        """Run the forward pass.
        Parameters
        ----------
        x : torch.Tensor
            Shape `(n_samples, in_chans, img_size, img_size)`.
        Returns
        -------
        logits : torch.Tensor
            Logits over all the classes - `(n_samples, n_classes)`.
        """
        n_samples = x.shape[0]
        x = self.patch_embed(x)

        cls_token = self.cls_token.expand(
                n_samples, -1, -1
        )  # (n_samples, 1, embed_dim)
        x = torch.cat((cls_token, x), dim=1)  # (n_samples, 1 + n_patches, embed_dim)
        x = x + self.pos_embed  # (n_samples, 1 + n_patches, embed_dim)
        x = self.pos_drop(x)

        for block in self.blocks:
            x = block(x)

        x = self.norm(x)

        cls_token_final = x[:, 0]  # just the CLS token
        x = self.head(cls_token_final)

        return x