# Do a finetuned ViT (patch8, img 224) forward pass of one image tensor of dimensions (Batch, Channels, Height, Width) = (1, 3, 224, 224)
# which gives us (224/8)**2 = 784 patches

# Those patches will be embedded individually, resulting in 784 embeddings of dimension 768 each i.e 784 tokens
# Making it 785 tokens with the class embedding token (often referenced as "CLS token") that is concatenated to the other ones whose final value is utilized to generate predictions

# Each embedding is passed in input to the number of transformer heads i.e 12, eventually focusing on different parts, patches of the images
# Each q k v is 64 dimensional

# Via this wrapper of the last layer of multi headed self attention (MSA), we can extract attention scores between sequences elements
# In this example, the query tensor shape is [1, 12, 785, 64] : batch size 1, 12 heads, 785 rows of 64 dimensional queries

# Code comments in french for reader's faster comprehension


import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
from timm.models import create_model
from torchvision.transforms import Compose, Resize, CenterCrop, Normalize, ToTensor


def to_tensor(img):
    transform_fn = Compose([Resize(249, 3), CenterCrop(224), ToTensor(), Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
    return transform_fn(img)

def show_img(img):
    img = np.asarray(img)
    plt.figure(figsize=(10, 10))
    plt.imshow(img)
    plt.axis('off')
    plt.show()

def show_img2(img1, img2, alpha=0.8):
    img1 = np.asarray(img1)
    img2 = np.asarray(img2)
    plt.figure(figsize=(10, 10))
    plt.imshow(img1)
    plt.imshow(img2, alpha=alpha)
    plt.axis('off')
    plt.show()

    
def my_forward_wrapper(attn_obj):
    def my_forward(x):
        
        B, N, C = x.shape # batchsize 1 dans le forward représente : les 1 positions+ 784 lignes de tokens qui ont chacun un embedding de dimension 768 pr les 3channels
        print('x.shape', x.shape) # [1, 785, 768] 
        
        qkv = attn_obj.qkv(x).reshape(B, N, 3, attn_obj.num_heads, C // attn_obj.num_heads).permute(2, 0, 3, 1, 4) # Pour les 12 heads on a, pour les 785 tokens, nos q,k,v de dim 64; avec batchsize=1
        print("qkv shape", qkv.shape) # torch.Size([3, 1, 12, 785, 64])
        
        q, k, v = qkv.unbind(0)  # Découpe le tenseur en 3 tuples selon la dimension0 puis suppr cette dimension
        print("q shape", q.shape) # [1, 12, 785, 64]
        
        

        attn = (q @ k.transpose(-2, -1)) * attn_obj.scale
        attn = attn.softmax(dim=-1)
        attn = attn_obj.attn_drop(attn) # Shape attn [1, 12, 785, 785]    
        
        attn_obj.attn_map = attn
        print('Shape attn_map', attn_obj.attn_map.shape) # [1, 12, 785, 785] pour chaque tete et chaque token on a le score d'attention relatif à tout les autres tokens
        
        attn_obj.cls_attn_map = attn[:, :, 0, 1:]
        print('Shape cls_attn_map', attn_obj.cls_attn_map.shape) #[1, 12, 784] pour chaque tete on a les 784 indications de classes du patch

        x = (attn @ v).transpose(1, 2).reshape(B, N, C) #[1, 785, 768]
        print("embeddings pondérés avec les scores d'att, de similarité",x.shape) 
        x = attn_obj.proj(x) # Transformation linéaire qui concatène les Z, matmul par WO
        x = attn_obj.proj_drop(x)
        
        return x
    return my_forward


# # References
# https://www.kaggle.com/code/piantic/vision-transformer-vit-visualize-attention-map/notebook#Visualize-Attention-Map
# https://github.com/rwightman/pytorch-image-models/discussions/1232
# https://github.com/jeonsworld/ViT-pytorch/blob/main/visualize_attention_map.ipynb
