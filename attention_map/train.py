import os
import numpy as np
import torch
import torch.nn.functional as F

from tqdm import tqdm
from sklearn.metrics import accuracy_score, f1_score

def train_one_epoch(model, train_loader, loss_fn, optimizer):
    model.train()
    total_classes = []
    total_preds = []
    res = []
    
    t = tqdm(train_loader, desc = 'Train epoch')

    for batch_idx, (imgs, classes) in enumerate(t):
        imgs = imgs.cuda()
        classes = classes.cuda()
        preds = model(imgs)

        loss = loss_fn(preds, classes)
        if batch_idx % 100 == 0:
            print("Loss: ", loss)
            
        preds = torch.argmax(F.softmax(preds, dim=1), axis=1)
        res.append((classes.cpu().detach(), preds.cpu().detach()))
        
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
   
    total_classes = torch.cat([entry[0] for entry in res], 0)
    total_preds = torch.cat([entry[1] for entry in res], 0)
    
    # Moyenne harmonique entre precision et recall
    f1_score_epoch = f1_score(total_classes.numpy(), total_preds.numpy(), average='macro')
    print("f1_score", f1_score_epoch)
    accuracy_epoch = accuracy_score(total_classes.numpy(), total_preds.numpy())
    print("accuracy_score", accuracy_epoch)
    
    return f1_score_epoch

def test_one_epoch(model, test_loader, loss_fn):
    model.eval()  
    total_classes = []
    total_preds = []

    t = tqdm(test_loader, desc="Test epoch")
    for batch_idx, (imgs, classes) in enumerate(t):

        imgs = imgs.cuda()
        classes = classes.cuda()
        preds = model(imgs)

        loss = loss_fn(preds, classes)

        preds = np.argmax(F.softmax(preds, dim=1).cpu().detach().numpy(), axis=1)

        total_classes += classes.cpu().numpy().tolist()
        total_preds += preds.tolist()


    f1_score_epoch = f1_score(total_classes, total_preds, average = 'macro') # Class weighted possible
    accuracy = accuracy_score(total_classes, total_preds)
    
    print('Test f1_score: {:.2f}, accuracy: {:.2f}, '.format(f1_score_epoch*100, accuracy*100))  


# If CUDA out of memory on kaggle, try saving weights after ViT training
# and then reloading them in another notebook for testing

"""

def trainable_params():
        for param in model.parameters():
            if param.requires_grad:
                yield param
num_trainable_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
print('num_trainable_params:', num_trainable_params)

loss_fn = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(trainable_params(), lr= 0.0001, weight_decay = 1e-5)

save_dir = './'
num_epochs = 1

for epoch in range(num_epochs):
    train_one_epoch(model, train_loader, loss_fn, optimizer)
    model_state = {
        'epoch': epoch+1,
        'state_dict': model.state_dict(),
    }
    torch.save(model_state, os.path.join(save_dir, \
                'checkpoint_%d.pth.tar' % num_epochs))

from IPython.display import FileLink
FileLink(r'./checkpoint_1.pth.tar')
"""