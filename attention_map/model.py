import numpy as np
import torch
import timm

# Finetuning a ViT from timm 

def select_pretrained_timm_model(m):
    
    num_finetune_classes = 10
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    model = timm.create_model(m, pretrained=True, num_classes=num_finetune_classes).to(device)
    
    return model

# Room to further define new architectures