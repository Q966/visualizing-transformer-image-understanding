
import argparse
import numpy as np
from PIL import Image
import torch
import torch.nn as nn
import torch.backends.cudnn as cudnn
import torch.nn.functional as F
from torchvision.datasets import ImageFolder
import torchvision.transforms as transforms
from torch.utils.data import DataLoader, random_split, Subset, SubsetRandomSampler
from sklearn.model_selection import train_test_split

from model import select_pretrained_timm_model
from train import train_one_epoch, test_one_epoch
from attention_map import to_tensor, show_img, show_img2, my_forward_wrapper

#EuroSAT est un dataset pour la classification d'image satellites, ici 27k quasi équilibré en RGB 64x64 
# data path Kaggle
data_path='../input/eurosatrgb/EuroSAT/2750'


class UnNormalize(object):
    def __init__(self, mean, std):
        self.mean = mean
        self.std = std
    def __call__(self, tensor):
        for t, m, s in zip(tensor, self.mean, self.std):
            t.mul_(s).add_(m)
        return tensor

# Possiblement differentes transformations pour train et test, et séparer le dataset mais le focus ici est l'obtention de l'attention map
normalize = transforms.Normalize(mean = [0.5, 0.5, 0.5], std = [0.5, 0.5, 0.5])

transf = transforms.Compose([UnNormalize(normalize.mean, normalize.std), 
                            transforms.ToPILImage()])


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--image_dir',
            default = '../data',
            help='image directory')
    

    args = parser.parse_args()

    args.image_dir = data_path

    train_transform = transforms.Compose([
        transforms.Resize(224),
        transforms.RandomHorizontalFlip(), 
        transforms.ToTensor(),
        normalize])

    test_transform = transforms.Compose([
        transforms.Resize(224),
        transforms.RandomVerticalFlip(),
        transforms.ToTensor(),
        normalize])

    dataset = ImageFolder(root = data_path, transform = train_transform)
    #Les 10 classes sont les suivantes
    print(dataset.classes)

    targets = dataset.targets
    train_idx, test_idx= train_test_split(
    np.arange(len(targets)),
    test_size=0.1,
    shuffle=True,
    stratify=targets)


    train_data = Subset(dataset, train_idx)
    test_data = Subset(dataset, test_idx)
    print(f'\n First train dataset image tensor shape : {train_data[0][0].shape},\n class: {train_data[0][1]}')

    batch_size = 16
    train_loader = DataLoader(train_data, batch_size= batch_size, shuffle= True)
    test_loader = DataLoader(test_data, batch_size= batch_size, shuffle= True)
    print(f"\n Train {len(train_data)}, test {len(test_data)}") 

    # Define model
    model = select_pretrained_timm_model('vit_base_patch8_224')

    def trainable_params():
            for param in model.parameters():
                if param.requires_grad:
                    yield param
    num_trainable_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
    print('num_trainable_params:', num_trainable_params)



    # Loading weights and 1 epoch model testing

    """
    checkpoint = torch.load('../input/eurosatrgb/traintestsplit_checkpoint_1.pth.tar')
    model_dict = model.state_dict()
    finetuned_dict = {k: v for k, v in checkpoint['state_dict'].items() if k in model_dict}
    model_dict.update(finetuned_dict)
    model.load_state_dict(model_dict)

    test_one_epoch(model, test_loader, loss_fn)
    """

    checkpoint = torch.load('../input/eurosatrgb/checkpoint_1.pth.tar')
    model_dict = model.state_dict()
    finetuned_dict = {k: v for k, v in checkpoint['state_dict'].items() if k in model_dict}
    model_dict.update(finetuned_dict)
    model.load_state_dict(model_dict)


    model.eval()
    img = '../input/eurosatrgb/EuroSAT/2750/River/River_1000.jpg'
    img = Image.open(img)
    x = to_tensor(img)
    x = x.cuda()

    print("\nShape tenseur image redimensionnée, recadrée et normalisée:", x.shape)

    print("\n Dernier block/couche d'attention du ViT: \n", model.blocks[-1].attn)

    # Last attention layer of the architecture
    model.blocks[-1].attn.forward = my_forward_wrapper(model.blocks[-1].attn)



    y = model(x.unsqueeze(0))
    attn_map = model.blocks[-1].attn.attn_map.mean(dim=1).squeeze(0).detach().cpu() # Moyenne sur les 12 tetes: shape [785, 785]
    print("Shape mean squeezed class attention map", attn_map.shape)

    cls_weight = model.blocks[-1].attn.cls_attn_map.mean(dim=1).view(28, 28).detach().cpu() # Moyenne sur les tetes et vue du cls token 28x28
    print("Shape mean cls_weight", cls_weight.shape)

    img_resized = x.permute(1, 2, 0) * 0.5 + 0.5
    cls_resized = F.interpolate(cls_weight.view(1, 1, 28, 28), (224, 224), mode='bilinear').view(224, 224, 1)
    # Vue (N, C, dims de sortie reshaped)



    show_img(img)
    show_img(attn_map)
    show_img(cls_weight)

    show_img(img_resized.cpu())
    show_img2(img_resized.cpu(), cls_resized.cpu(), alpha=0.5)

if __name__== "__main__":
    main()